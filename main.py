import json


def displaydata(jsondata):
    if isinstance(jsondata, dict):
        for k, v in jsondata.items():
            if isinstance(v, dict):
                displaydata(v)
            elif hasattr(v, '__iter__') and not isinstance(v, str):
                for items in v:
                    displaydata(items)
            elif isinstance(v, str):
                if k == "color":
                    list_color.append(v)
            else:
                if k == "color":
                    list_color.append(v)
    return list_color


file_object = open("scripts/core/jsondata.json", )
json_data = json.load(file_object)
list_color = []
d = dict(json_data)
list_obtained = displaydata(d)
u_list = list(set(list_obtained))
print(len(u_list))
print(u_list)
